<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>TEST</title>
        <!-- <link rel="stylesheet" href="/css/style.css" /> -->
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    </head>
    
    <body>
        <!-- Facebook SDK -->
        <div id="fb-root"></div>
        <script>
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&appId=653532931392296&version=v2.0";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
                        
        <h1>Page de test du documathon</h1>

        <!-- Facebook form -->
        <form method="post" action="index.php">
            <p>
                Voici les tweets proposés.
            </p>

		    <p>
                Quel type de message voulez-vous envoyer ?<br />
                <input type="radio" name="fb_message" value="0" id="0" /> <label for="0"><strong>1 :</strong> "La documentation du projet <?php echo $projectname ?> a été mise à jour, venez voir les nouveautés sur : http://wiki.faclab.com/index.php?title=<?php echo str_replace(' ', '_', $projectname) ?>."</label><br />
                <input type="radio" name="fb_message" value="1" id="1" /> <label for="1"><strong>2 :</strong> "Nouvelle étape du projet <?php echo $projectname ?> de <?php echo $projectauthor ?> à : <?php echo $projectlaststepdescription ?>. Machine utilisée : <?php echo $projectlaststepfirstmachine ?>."</label><br />
                <input type="radio" name="fb_message" value="2" id="2" /> <label for="2"><strong>3 :</strong> "Au Faclab aujourd'hui <?php echo $projectname ?> : <?php echo $projectshortdescription ?>"</label>
            </p>

		   <input type="submit" value="Valider" />
		</form>

        <p>
            Vous pouvez aussi tweeter sur votre propre compte !
        </p>

        <!-- Script for the facebook button -->
        <div class="fb-share-button" data-href="http://wiki.faclab.com/index.php?title=<?php echo str_replace(' ', '_', $projectname) ?>" data-type="button"></div>

    </body>
</html>