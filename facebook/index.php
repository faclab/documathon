<?php

    // fonction d'environnement PHP permet de notifier que le script est en mode debug et surchage la configuration apache pour afficher les erreurs
    ini_set('display_errors', 'On');
    error_reporting(E_ALL);

    // We include the data access
    include '../access_data.php';

    //------------------------------------------- OPEN FACEBOOK CODE ---------------------------------------------
    // include required files form Facebook SDK
     
    // Facebook PHP SDK v4.0.8
 
    // path of these files have changes
    require_once( 'Facebook/HttpClients/FacebookHttpable.php' );
    require_once( 'Facebook/HttpClients/FacebookCurl.php' );
    require_once( 'Facebook/HttpClients/FacebookCurlHttpClient.php' );
     
    require_once( 'Facebook/Entities/AccessToken.php' );
    require_once( 'Facebook/Entities/SignedRequest.php' );
     
    // other files remain the same
    require_once( 'Facebook/FacebookSession.php' );
    require_once( 'Facebook/FacebookRedirectLoginHelper.php' );
    require_once( 'Facebook/FacebookRequest.php' );
    require_once( 'Facebook/FacebookResponse.php' );
    require_once( 'Facebook/FacebookSDKException.php' );
    require_once( 'Facebook/FacebookRequestException.php' );
    require_once( 'Facebook/FacebookOtherException.php' );
    require_once( 'Facebook/FacebookAuthorizationException.php' );
    require_once( 'Facebook/GraphObject.php' );
    require_once( 'Facebook/GraphSessionInfo.php' );
     
    // path of these files have changes
    use Facebook\HttpClients\FacebookHttpable;
    use Facebook\HttpClients\FacebookCurl;
    use Facebook\HttpClients\FacebookCurlHttpClient;
     
    use Facebook\Entities\AccessToken;
    use Facebook\Entities\SignedRequest;
     
    // other files remain the same
    use Facebook\FacebookSession;
    use Facebook\FacebookRedirectLoginHelper;
    use Facebook\FacebookRequest;
    use Facebook\FacebookResponse;
    use Facebook\FacebookSDKException;
    use Facebook\FacebookRequestException;
    use Facebook\FacebookOtherException;
    use Facebook\FacebookAuthorizationException;
    use Facebook\GraphObject;
    use Facebook\GraphSessionInfo;


        //--------------------------------------------- VARIABLES -------------------------------------------------------
        // Facebook Key
        $app_ID = '653532931392296';
        $app_secret = 'b76d08d29119c12f148554b4a257abd2';
        $url = 'http://documathon.faclab.org/facebook/index.php';
        $access_right = 'publish_actions';
        $pageID = '792530177432064';

        //--------------------------------------------- SESSION ---------------------------------------------------------
        // start session
        session_start();
         
        // init app with app id and secret
        FacebookSession::setDefaultApplication( $app_ID, $app_secret );
         
        // login helper with redirect_uri
        $helper = new FacebookRedirectLoginHelper( $url );

        // generate login url with scope, each permission as element in array
        $loginUrl = $helper->getLoginUrl( array($access_right) );
         
        // see if a existing session exists
        if ( isset( $_SESSION ) && isset( $_SESSION['fb_token'] ) ) {
            // create new session from saved access_token
            $session = new FacebookSession( $_SESSION['fb_token'] );

            var_dump($session);
            // validate the access_token to make sure it's still valid
            try {
                if ( !$session->validate() ) {
                    $session = null;
                }
            } catch ( Exception $e ) {
                // catch any exceptions
                $session = null;
            }
        }  
         
        if ( !isset( $session ) || $session === null ) {
            // no session exists

            try {
                $session = $helper->getSessionFromRedirect();
            } catch( FacebookRequestException $ex ) {
                // When Facebook returns an error
                // handle this better in production code
                print_r( $ex );
            } catch( Exception $ex ) {
                // When validation fails or other local issues
                // handle this better in production code
                print_r( $ex );
            }
          
        }

        //-------------------------------------------- CREATION MESSAGE STEP -----------------------------------------------
        // variables
        $messages = [];

        //1 - Create the first tweet
        $messages[0] = 'La documentation du projet '.$projectname.' a été mise à jour, venez voir les nouveautés sur le wiki !';

        //2 - Create the second tweet
        $messages[1] = 'Nouvelle étape du projet '.$projectname.' de '.$projectauthor.' sur l\'étape : '.$projectlaststepdescription.'. Les machines utilisées : '.$projectlaststepfirstmachine.'.';
       
        //3 - Create the third tweet
        $messages[2] = 'Au Faclab aujourd\'hui '.$projectname.' : '.$projectshortdescription;

        var_dump($session);
        //-------------------------------------------- SENDING MESSAGE STEP -----------------------------------------------
        // see if we have a session
        if ( isset( $session ) ) {
          
            // save the session
            $_SESSION['fb_token'] = $session->getToken();
            // create a session using saved token or the new one we generated at login
            $session = new FacebookSession( $session->getToken() );

            // graph api request for user accounts data (which page he have)
            $graphObject = (new FacebookRequest( $session, 'GET', '/me/accounts'))->execute()->getGraphObject()->asArray();

            // print profile data for checking
            echo '<pre>' . print_r( $graphObject, 1 ) . '</pre>';
            die();

            // // take the page token for the good page (make a check before to see the good page)
            // $page_number = 0

            // $page_token = $graphObject['data'][$page_number]->access_token;

            // // print to see the page token
            // // echo '<p>Page token : '.$page_token.'</p>';

            // // we change the user token to page token
            // $session = new FacebookSession($page_token);

            // If form is sent
            // if (isset($_POST['fb_message'])) {

            //      // Variables from form
            //     $fb_message = $_POST['fb_message'];

            // // /* make the API call */
            // // $request = new FacebookRequest(
            // //                     $session,
            // //                     'POST',
            // //                     '/'.$pageID.'/feed',
            // //                     array (
            // //                         'message' => $messages[$fb_message],
            // //                         )
            // //                     );

            
            // // $response = $request->execute();
            // // $graphObject = $response->getGraphObject();

            // // /* handle the result */
            // // // print profile data
            // // echo '<pre>' . print_r( $graphObject, 1 ) . '</pre>';

            // }

            // // print logout url using session and redirect_uri (logout.php page should destroy the session)
            // echo '<a href="' . $helper->getLogoutUrl( $session, 'http://yourwebsite.com/app/logout.php' ) . '">Se déconnecter</a>';

        } else {
          // show login url
          echo '<a href="' . $helper->getLoginUrl( array( 'email', 'user_friends' ) ) . '">Cliquer pour connecter le documathon</a>';
        }

        // // see if we have a session
        // if ( isset( $session ) ) {

        //     // save the session
        //     $_SESSION['FBRLH_state'] = $session->getToken();

        //     // create a session using saved token or the new one we generated at login
        //     $session = new FacebookSession($session->getToken());


        //     //------------------------------------------- CONSTRUCT THE REQUEST -------------------------------------------
        //     // We can check the response
        //     try {

        //         // We construct the request and send it
        //         $response = (new FacebookRequest(
        //         $session, 'POST', '/'.$pageID.'/feed', array(
        //         'link' => 'http://wiki.faclab.com/index.php?title='.str_replace(' ', '_', $projectname),
        //         'message' => $messages[$fb_message],
        //         'access_token' => $access_token
        //         )
        //         ))->execute()->getGraphObject()->asArray();

        //         // id of response
        //         echo "Posted with id: " . $response['id'];

        //     } catch(FacebookRequestException $e) {

        //         // if we have an error
        //         echo "Exception occured, code: " . $e->getCode();
        //         echo " with message: " . $e->getMessage();

        //     }  
        // }

    // include 'view.php';

    // response if tweet is send
    if(isset($graphObject['id'])) {

        echo '<p>Le message <strong>'.($message + 1).'</strong> a été envoyé !</p>';
        echo '<p>facebook a posté le message sour l\'id :'.$graphObject['id'].'</p>';

    }
?>
