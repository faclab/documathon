<?php

    ini_set('display_errors', 'On');
    error_reporting(E_ALL);

    // path of these files have changes
    require_once( 'Facebook/HttpClients/FacebookHttpable.php' );
    require_once( 'Facebook/HttpClients/FacebookCurl.php' );
    require_once( 'Facebook/HttpClients/FacebookCurlHttpClient.php' );
     
    require_once( 'Facebook/Entities/AccessToken.php' );
    require_once( 'Facebook/Entities/SignedRequest.php' );
     
    // other files remain the same
    require_once( 'Facebook/FacebookSession.php' );
    require_once( 'Facebook/FacebookRedirectLoginHelper.php' );
    require_once( 'Facebook/FacebookRequest.php' );
    require_once( 'Facebook/FacebookResponse.php' );
    require_once( 'Facebook/FacebookSDKException.php' );
    require_once( 'Facebook/FacebookRequestException.php' );
    require_once( 'Facebook/FacebookOtherException.php' );
    require_once( 'Facebook/FacebookAuthorizationException.php' );
    require_once( 'Facebook/GraphObject.php' );
    require_once( 'Facebook/GraphSessionInfo.php' );
     
    // path of these files have changes
    use Facebook\HttpClients\FacebookHttpable;
    use Facebook\HttpClients\FacebookCurl;
    use Facebook\HttpClients\FacebookCurlHttpClient;
     
    use Facebook\Entities\AccessToken;
    use Facebook\Entities\SignedRequest;
     
    // other files remain the same
    use Facebook\FacebookSession;
    use Facebook\FacebookRedirectLoginHelper;
    use Facebook\FacebookRequest;
    use Facebook\FacebookResponse;
    use Facebook\FacebookSDKException;
    use Facebook\FacebookRequestException;
    use Facebook\FacebookOtherException;
    use Facebook\FacebookAuthorizationException;
    use Facebook\GraphObject;
    use Facebook\GraphSessionInfo;



    require_once( 'SammyK/FacebookQueryBuilder/FQB.php' );
    require_once( 'SammyK/FacebookQueryBuilder/FacebookQueryBuilderException.php' );

	use SammyK\FacebookQueryBuilder\FQB;
	use SammyK\FacebookQueryBuilder\FacebookQueryBuilderException;


	// Facebook Key
	$app_ID = '653532931392296';
	$app_secret = 'b76d08d29119c12f148554b4a257abd2';




	FQB::setAppCredentials($app_ID, $app_secret);

	$fqb = new FQB();

	session_start();


	try
	{
	    $token = $fqb->auth()->getTokenFromRedirect('http://documathon.faclab.org/facebook/facebook2.php');
	}
	catch (FacebookQueryBuilderException $e)
	{
	    echo '<p>Error: ' . $e->getMessage() . "\n\n";
	    echo '<p>Facebook SDK Said: ' . $e->getPrevious()->getMessage() . "\n\n";
	    exit;
	}

	if ( ! $token)
	{
	    /**
	     * No token returned. Show login link.
	     */
	    $scope = ['email', 'read_stream', 'publish_actions']; // Optional
	    $login_url = $fqb->auth()->getLoginUrl('http://documathon.faclab.org/facebook/facebook2.php', $scope);
	    echo '<a href="' . $login_url . '">Log in with Facebook</a>';
	    exit;
	}

	echo '<h1>Returned AccessToken Object</h1>' . "\n\n";
	var_dump($token);

	/**
	 * Get info about the access token.
	 */
	try
	{
	    $token_info = $token->getInfo();
	}
	catch (FacebookQueryBuilderException $e)
	{
	    echo '<p>Error: ' . $e->getMessage() . "\n\n";
	    echo '<p>Facebook SDK Said: ' . $e->getPrevious()->getMessage() . "\n\n";
	    echo '<p>Graph Said: ' .  "\n\n";
	    var_dump($e->getResponse());
	    exit;
	}

	var_dump($token_info->toArray());

	if ( ! $token->isLongLived())
	{
	    /**
	     * Extend the access token.
	     */
	    try
	    {
	        $token = $token->extend();
	    }
	    catch (FacebookQueryBuilderException $e)
	    {
	        echo '<p>Error: ' . $e->getMessage() . "\n\n";
	        echo '<p>Facebook SDK Said: ' . $e->getPrevious()->getMessage() . "\n\n";
	        echo '<p>Graph Said: ' .  "\n\n";
	        var_dump($e->getResponse());
	        exit;
	    }

	    echo '<h1>Long-lived AccessToken Object</h1>' . "\n\n";
	    var_dump($token);

	    /**
	     * Get info about the access token.
	     */
	    try
	    {
	        $token_info = $token->getInfo();
	    }
	    catch (FacebookQueryBuilderException $e)
	    {
	        echo '<p>Error: ' . $e->getMessage() . "\n\n";
	        echo '<p>Facebook SDK Said: ' . $e->getPrevious()->getMessage() . "\n\n";
	        echo '<p>Graph Said: ' .  "\n\n";
	        var_dump($e->getResponse());
	        exit;
	    }

	    var_dump($token_info->toArray());
	}

	FQB::setAccessToken($token);

	/**
	 * Get the logged in user's profile.
	 */
	try
	{
	    $user = $fqb->object('me')->get();
	}
	catch (FacebookQueryBuilderException $e)
	{
	    echo '<p>Error: ' . $e->getMessage() . "\n\n";
	    echo '<p>Facebook SDK Said: ' . $e->getPrevious()->getMessage() . "\n\n";
	    echo '<p>Graph Said: ' .  "\n\n";
	    var_dump($e->getResponse());
	    exit;
	}

	echo '<h1>User Data</h1>' . "\n\n";
	var_dump($user->toArray());



?>