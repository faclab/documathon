<?php

    ini_set('display_errors', 'On');
    error_reporting(E_ALL);

 
// include required files form Facebook SDK
 
    // path of these files have changes
    require_once( 'Facebook/HttpClients/FacebookHttpable.php' );
    require_once( 'Facebook/HttpClients/FacebookCurl.php' );
    require_once( 'Facebook/HttpClients/FacebookCurlHttpClient.php' );
     
    require_once( 'Facebook/Entities/AccessToken.php' );
    require_once( 'Facebook/Entities/SignedRequest.php' );
     
    // other files remain the same
    require_once( 'Facebook/FacebookSession.php' );
    require_once( 'Facebook/FacebookRedirectLoginHelper.php' );
    require_once( 'Facebook/FacebookRequest.php' );
    require_once( 'Facebook/FacebookResponse.php' );
    require_once( 'Facebook/FacebookSDKException.php' );
    require_once( 'Facebook/FacebookRequestException.php' );
    require_once( 'Facebook/FacebookOtherException.php' );
    require_once( 'Facebook/FacebookAuthorizationException.php' );
    require_once( 'Facebook/GraphObject.php' );
    require_once( 'Facebook/GraphSessionInfo.php' );
     
    // path of these files have changes
    use Facebook\HttpClients\FacebookHttpable;
    use Facebook\HttpClients\FacebookCurl;
    use Facebook\HttpClients\FacebookCurlHttpClient;
     
    use Facebook\Entities\AccessToken;
    use Facebook\Entities\SignedRequest;
     
    // other files remain the same
    use Facebook\FacebookSession;
    use Facebook\FacebookRedirectLoginHelper;
    use Facebook\FacebookRequest;
    use Facebook\FacebookResponse;
    use Facebook\FacebookSDKException;
    use Facebook\FacebookRequestException;
    use Facebook\FacebookOtherException;
    use Facebook\FacebookAuthorizationException;
    use Facebook\GraphObject;
    use Facebook\GraphSessionInfo;

// Facebook Key
$app_ID = '653532931392296';
$app_secret = 'b76d08d29119c12f148554b4a257abd2';
$url = 'http://documathon.faclab.org/facebook/facebook.php';
$access_right = 'publish_actions';
$pageID = '792530177432064';

// start session
session_start();
 
// init app with app id and secret
FacebookSession::setDefaultApplication( $app_ID, $app_secret );
 
// login helper with redirect_uri
$helper = new FacebookRedirectLoginHelper( $url );

// generate login url with scope, each permission as element in array
$loginUrl = $helper->getLoginUrl( array($access_right) );

// see if a existing session exists
if ( isset( $_SESSION ) && isset( $_SESSION['fb_token'] ) ) {
  // create new session from saved access_token
  $session = new FacebookSession( $_SESSION['fb_token'] );
  
  // validate the access_token to make sure it's still valid
  try {
    if ( !$session->validate() ) {
      $session = null;
    }
  } catch ( Exception $e ) {
    // catch any exceptions
    $session = null;
  }
}  
 
if ( !isset( $session ) || $session === null ) {
  // no session exists
  
  try {
    $session = $helper->getSessionFromRedirect();
  } catch( FacebookRequestException $ex ) {
    // When Facebook returns an error
    // handle this better in production code
    print_r( $ex );
  } catch( Exception $ex ) {
    // When validation fails or other local issues
    // handle this better in production code
    print_r( $ex );
  }
  
}

// see if we have a session
if ( isset( $session ) ) {
  
  // save the session
  $_SESSION['fb_token'] = $session->getToken();
  // create a session using saved token or the new one we generated at login
  $session = new FacebookSession( $session->getToken() );
  
  // graph api request for user data
  $graphObject = (new FacebookRequest( $session, 'GET', '/me/accounts'))->execute()->getGraphObject()->asArray();
  
  // print profile data
  echo '<pre>' . print_r( $graphObject, 1 ) . '</pre>';
  
  $page_token = $graphObject['data'][0]->access_token;

  echo '<p>Page token : '.$page_token.'</p>';

  $session = new FacebookSession($page_token);

  	/* make the API call */
	$request = new FacebookRequest(
	  $session,
	  'POST',
	  '/'.$pageID.'/feed',
	  array (
	    'message' => 'This is a test message',
	  )
	);
	$response = $request->execute();
	$graphObject = $response->getGraphObject();

	/* handle the result */
	// print profile data
  echo '<pre>' . print_r( $graphObject, 1 ) . '</pre>';
  
  // print logout url using session and redirect_uri (logout.php page should destroy the session)
  echo '<a href="' . $helper->getLogoutUrl( $session, 'http://yourwebsite.com/app/logout.php' ) . '">Logout</a>';

} else {
  // show login url
  echo '<a href="' . $helper->getLoginUrl( array( 'email', 'user_friends' ) ) . '">Login</a>';
}

?>