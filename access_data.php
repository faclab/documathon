<?php

	//------------------------------ 1 -- open and parse the JSON ----------------------------------------------------
	$json = file_get_contents("donnees.json");

	$parsed_json = json_decode($json);

	// We take the step number
	$StepNumber = count($parsed_json->{'projet'}->{'étapes'});

	//------------------------------- 2 -- take informations of the project ---------------------------------------
    $projectname = $parsed_json->{'projet'}->{'nom'};

    $projectauthor = $parsed_json->{'projet'}->{'auteur'};

    $projectshortdescription = $parsed_json->{'projet'}->{'descriptif court'};

    $projectlaststepdescription = $parsed_json->{'projet'}->{'étapes'}[$StepNumber - 1]->{'descriptif'};

    $projectlaststepfirstmachine = $parsed_json->{'projet'}->{'étapes'}[$StepNumber - 1]->{'machines'}[0]->{'nom'};